
#set -x
#!/bin/bash
#
#################################################
# Super script pour recevoir un message lorsque votre centre de vaccination reçoit des doses !
#
# 0,15,30,45 * * * * /covid/covid.sh
#
# Merci qui ?
#################################################

# Variables à adatper : 
# Adresse mail sur laquelle vous souhaitez recevoir l'info
email='nom@domaine.fr'
# recevoir tous les mail ou uniquement lorsqu'il y a des vaccins dispo ?
critere='TOUS'  #Valeurs autorisées TOUS ou DISPO
# URL correspondant à votre recherche. Aller sur sante.fr, choisir le centre, faites F12, selectionner le type de vaccin, et relevez l'adresse complète qui est soumise
urldocto='https://partners.doctolib.fr/availabilities.json?start_date=2021-03-26&visit_motive_ids=2547158&agenda_ids=409977&insurance_sector=public&practice_ids=164463&destroy_temporary=true&limit=6'


rm /tmp/res.txt

curl -s ${urldocto} \
  -H 'sec-ch-ua: "Google Chrome";v="89", "Chromium";v="89", ";Not A Brand";v="99"' \
  -H 'accept: application/json' \
  -H 'Referer: https://partners.doctolib.fr/espic-etablissement-de-sante-prive-d-interet-collectif/l-isle-adam/centre-de-vaccination-covid19-de-l-isle-adam-hopital-chantepie-mancier' \
  -H 'sec-ch-ua-mobile: ?0' \
  -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36' \
  -H 'content-type: application/json; charset=utf-8' \
  --compressed >/tmp/res.txt

combien=`grep -c "total\":0" /tmp/res.txt`
if [ "$combien" -eq "1" ]; then 
  if [ $critere = "TOUS" ]; then
(cat - /tmp/res.txt)<<EOF | /usr/sbin/sendmail -i $email
Subject: "Pas de vaccins"
To: $email

EOF
  fi
else
  sujet="Allez voir la page ${urldocto} , il y a du nouveau !!!"

(cat - /tmp/res.txt)<<EOF | /usr/sbin/sendmail -i $email
Subject: $sujet
To: $email

EOF
fi
